# ESTB GameJam - Cyborg Jäger

## Explanation

After building a team-based devlelopment toolchain / pipeline for homebrew Sega Mega Drive development, using SGDK, I put it to the test with a game jam. I invited 6 friends who each had pre existing abilities in programming, art or design. Utilizing the established server and toolbox of ESTB, we held a game jam to create a funcitoning homebrew game for the Sega Mega Drive, within 12 hours.

Of note is the fact that no team member had ever worked with the chosen tools or had any experience with homebrew development whatsoever. Through good documentation and the streamlined nature of both the SGDK framework and my ESTB application, we were still abe to create a functioning game in a short amount of time. A result, which certainly wouldn't have been easily possible a few years ago!

The game itself is a very simple run and gun game in which the titular Cyborg has to hunt small, bouncy red monkeys. If the monkeys touch him, he looses a health point, if they are shot, they despawn. The map is limited, but can be traversed with jumping. If 15 monkeys are eliminated, the game is won and restarts. 

![Cyborg Jäger Game Console & Emulator](https://gitlab.com/EzoFuzz/ezos-sega-toolbox-estb/-/raw/main/__readme_assets/cj.png)

Fairly simple stuff, but considering we crammed in sprite art & animations, tilesets, paralax scrolling, music and precise collision detection, all while each team member had to learn their tools *(Aseprite, Deflemask, VSCode, etc.)* as they went, is decently impressive.
![Pi and Networking Seutp](https://gitlab.com/EzoFuzz/ezos-sega-toolbox-estb/-/raw/main/__readme_assets/ntw.png)

## Prerequesits for Building

The toolchain is focused around SGDK as a starting point, especially for it's resource compiler.
Setting up SGDK is quite simple. For Windows, simply follow the official instructions on how to setup SGDK. 

For Linux and Mac, ports and cross platform implementations exist and are certainly deployable. Our RaspberryPi Server ran a custom Docker SGDK and SNASM build for ARM, facilitating basic server-side build automation.

Compilation and asset management is all handled within SGDK itself, so no other dependencies should be needed to build the ROM.

For writing code, VSCode and the amazing Genesis Code Plugin are a no-brainer, trust me.