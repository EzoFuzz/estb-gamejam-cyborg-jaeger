#include <genesis.h>

#include <art.h>
#include <lvl.h>
#include <level.h>
#include <mus.h>

#include <state.h>

// Constant defines
#define WINDOW_V FALSE
#define WINDOW_POS 2
#define AMNT_BULLETS 5
#define AMNT_ENEMIES 10
#define GRAVITY FIX16(0.34)
#define ACCELL  FIX16(0.20)
#define DECCELL FIX16(0.30)
#define VEL_JMP FIX16(-5.8)
#define SPD_PLAYER_MAX FIX16(2.25)
#define SPD_ENEMY_ACC FIX16(0.1)
#define VEL_ENEMY_JMP FIX16(-2)
#define SPD_ENEMY_MAX FIX16(1.3)
#define SPD_BULLET 5
#define GOAL_SCORE 15
#define MAP_SOLID 0
#define MAP_SCR_W 320
#define MAP_SCR_H 240
#define MAP_MAX_W 1728
#define MAP_PLAYER_OFFSET 0
#define MAP_SPAWN_DISTANCE 120
#define INVUL_TIME 2

// Types
typedef struct {
    s16 iX;
    s16 iY;
} Vec2_int;
typedef struct {
    f32 fX;
    f32 fY;
} Vec2_fix;
typedef struct {
    // Physical values
    //Vec2_fix pos;
    Vec2_int pos;
    Vec2_fix vel;
    Vec2_int dimPx;
    Vec2_int dimTl;
    s8 dir;
    // Game Values
    u8 hp;
    Sprite *spr;
    u8 contact;
} Entity;

// Instances
Entity player;
Entity bullets[AMNT_BULLETS];
u8 bullets_active = 0;
Entity enemies[AMNT_ENEMIES];
u8 enemies_active = 0;

f32 invulTime = FIX32(0);
u8 isInvul = FALSE;
u8 blinkInvul = FALSE;

// Additional variables
Map   *map_cur;
u16   tile_index = TILE_USERINDEX;
s16   scroll_bg_index[4]   = { 3, 8, 12, 17 };
s16   scroll_bg_length[4]  = { 5, 4, 3,  15 };
fix16 scroll_bg_shift[4]   = { FIX16(0.5), FIX16(0.35), FIX16(0.2), FIX16(0.1) };
s16   scroll_bg_vali[32]   = { 0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0 };
fix16 scroll_bg_valf[32]   = { 0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0, 
                               0, 0, 0, 0, 0, 0, 0, 0 };
s32 mapPos = 0;
s32 mapPos_last = 0;
s8 player_last_dir = 0;
u8 player_score = 0;
char *scoreTxt;

// Define Auxiliary functions
bool colCheckMap(Entity *e, u8 hor, u8 ver);
void spawnEnemy();
void drawScore();
void drawHealth();
void fireGun();

// Input handlers
void input_game( u16 joy, u16 changed, u16 state) 
{
	if (joy == JOY_1)
	{
	if (state & BUTTON_RIGHT) {
            player.dir = 1;
            player_last_dir = 1;
            SPR_setHFlip(player.spr, FALSE);
		}
        if (state & BUTTON_LEFT) {
            player.dir = -1;
            player_last_dir = -1;
            SPR_setHFlip(player.spr, TRUE);
		}
        if ((changed & BUTTON_RIGHT) || (changed & BUTTON_LEFT)) {
            if (!(state & BUTTON_RIGHT) && !(state & BUTTON_LEFT)) {
                player.dir = 0;
            }
        }
        if ((player.contact) && (state & BUTTON_A)) {
            player.vel.fY = VEL_JMP;
		}
        if (state & BUTTON_B) {
            fireGun();
        }
	}
}

// Setup functions
void setup_game() {
    SYS_disableInts();

    // Setup inputs
    JOY_reset();
    JOY_setEventHandler(&input_game);

    // Setup palettes
    PAL_setPalette(PAL0, img_cloud01.palette->data, DMA);
    PAL_setPalette(PAL1, spr_char.palette->data, DMA);
    PAL_setPalette(PAL2, spr_enemy.palette->data, DMA);
    VDP_setTextPalette(PAL1);
    VDP_setBackgroundColor(38);

    // Setup Planes
    VDP_clearPlane(BG_A, TRUE);
    VDP_clearPlane(BG_B, TRUE);
    VDP_setPlaneSize(64,32,TRUE);
    VDP_setWindowVPos(WINDOW_V, WINDOW_POS);
    VDP_setScrollingMode(HSCROLL_TILE, VSCROLL_PLANE);
    VDP_setTextPlane(WINDOW);
    tile_index = TILE_USERINDEX;
    scoreTxt = MEM_alloc(sizeof(char)*14);
    player_score = 0;
    drawScore();

    // Setup Map
    VDP_loadTileSet(&tiles_main, tile_index, DMA);
    map_cur = MAP_create(&map_new, BG_A, TILE_ATTR_FULL(PAL0, FALSE, FALSE, FALSE, tile_index));
    MAP_scrollTo(map_cur, 0, 0);
    tile_index += tiles_main.numTile;

    // Setup Background
    VDP_loadTileSet(img_cloud01.tileset, tile_index, DMA);
    VDP_fillTileMapRectInc(BG_B, tile_index, 3, 8, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 11, 9, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 23, 8, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 30, 9, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 38, 9, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 50, 8, 4, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 58, 8, 4, 3);
    tile_index += img_cloud01.tileset->numTile;

    VDP_loadTileSet(img_cloud02.tileset, tile_index, DMA);
    VDP_fillTileMapRectInc(BG_B, tile_index, 8, 12, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 15, 13, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 20, 13, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 23, 12, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 30, 12, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 35, 13, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 41, 12, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 46, 13, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 50, 13, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 53, 12, 2, 2);
    VDP_fillTileMapRectInc(BG_B, tile_index, 60, 13, 2, 2);
    tile_index += img_cloud02.tileset->numTile;

    VDP_loadTileSet(img_cloud03.tileset, tile_index, DMA);
    VDP_fillTileMapRectInc(BG_B, tile_index, 5, 3, 5, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 18, 4, 5, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 25, 3, 5, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 38, 5, 5, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 49, 3, 5, 3);
    VDP_fillTileMapRectInc(BG_B, tile_index, 57, 4, 5, 3);
    tile_index += img_cloud03.tileset->numTile;

    VDP_loadTileSet(img_mountains.tileset, tile_index, DMA);
    VDP_drawImageEx(BG_B, &img_mountains, tile_index, 0, 17, FALSE, DMA);
    VDP_drawImageEx(BG_B, &img_mountains, tile_index, 32, 17, FALSE, DMA);
    tile_index += img_mountains.tileset->numTile;

    // Setup Music
    XGM_setLoopNumber(10);
    XGM_startPlay((const u8 *)&mus_menu);

    // Setup Sprites & Player
    SPR_init();
    player = (Entity){ { 80, 120 },
                       { FIX16(0),   FIX16(0) },
                       { 32, 32 },
                       { 4,  4  },
                        0,
                        3, (Sprite*)NULL, FALSE };
    player.spr = SPR_addSprite(&spr_char, player.pos.iX, player.pos.iY, TILE_ATTR(PAL1, FALSE, FALSE, FALSE));
    drawHealth();

    // Setup Bullets
    for (int i=0; i<AMNT_BULLETS; i++) {
        bullets[i] = (Entity) { { 0, -10 },
                                { FIX16(0), FIX16(0)   },
                                { 16, 8 },
                                { 2,  1 },
                                  1,
                                  0, (Sprite*)NULL, FALSE };
        bullets[i].spr = SPR_addSprite(&spr_blaster, 0, -10, TILE_ATTR(PAL2, FALSE, FALSE, FALSE));
        SPR_setVisibility(bullets[i].spr, HIDDEN);
    }

    // Setup Enemies
    for (int i=0; i<AMNT_ENEMIES; i++) {
        enemies[i] = (Entity) { { 0, -10 },
                                { FIX16(0), FIX16(0)   },
                                { 16, 8 },
                                { 2,  1 },
                                  1,
                                  0, (Sprite*)NULL, FALSE };
        enemies[i].spr = SPR_addSprite(&spr_enemy, 0, -10, TILE_ATTR(PAL2, FALSE, FALSE, FALSE));
        SPR_setVisibility(enemies[i].spr, HIDDEN);
    }
    SYS_enableInts();
}

// Update functions
void update_game() {
    // Check Player collision against map
    if (colCheckMap(&player, TRUE, FALSE)) {  
        player.vel.fX = FIX16(0);
    } 
    if (colCheckMap(&player, FALSE, TRUE)) {  
        player.vel.fY = FIX16(0);
    }  

    // Add horizontal player movement
    player.vel.fX += fix16Mul(ACCELL, FIX16(player.dir));

    // Apply physical influence
    if (player.dir == 0) {
        if (player.vel.fX>FIX16(0)) {
            player.vel.fX -= DECCELL;
            if (player.vel.fX<FIX16(0)) player.vel.fX = FIX16(0);
        } else {
            player.vel.fX += DECCELL;
            if (player.vel.fX>FIX16(0)) player.vel.fX = FIX16(0);
        }
    }
    player.vel.fY += GRAVITY;

    // Restrict movement speed
    if (player.vel.fX > SPD_PLAYER_MAX) player.vel.fX = SPD_PLAYER_MAX;
    if (player.vel.fX < -1 * SPD_PLAYER_MAX) player.vel.fX = -1 * SPD_PLAYER_MAX;

    // Apply player movement
    player.pos.iX += fix16ToInt(player.vel.fX);
    player.pos.iY += fix16ToInt(player.vel.fY);

    // Map position and relative sprite positioning
    mapPos = player.pos.iX-(MAP_SCR_W/2)+MAP_PLAYER_OFFSET;
    if (mapPos < 0) {
        mapPos = 0;
    } else {
        if (mapPos > MAP_MAX_W-MAP_SCR_W){
            mapPos = MAP_MAX_W-MAP_SCR_W;
        }
    }
    MAP_scrollTo(map_cur, mapPos, 0);
    SPR_setPosition(player.spr, player.pos.iX-mapPos, player.pos.iY);

    // Player Invul Timer
    if (isInvul) {
        if (getTimeAsFix32(TRUE) > invulTime) {
            SPR_setVisibility(player.spr, VISIBLE);
            isInvul = FALSE;
        } else {
            SPR_setVisibility(player.spr, (blinkInvul ? VISIBLE : HIDDEN));
            blinkInvul = !blinkInvul;
        }
    }

    // Check for Dead
    if (player.hp <= 0) {
        VDP_clearText(0,0,44);
        VDP_drawText("GAME OVER!", 14, 0);
        waitMs(3000);
        VDP_clearPlane(WINDOW, TRUE);
        SPR_reset();
        SPR_end();
        gameState = ID_TITLE;
        return;
    }

    // Check for Win
    if (player_score >= GOAL_SCORE) {
        VDP_clearText(0,0,44);
        VDP_drawText("WINNER WINNER CHICKEN DINNER", 6, 0);
        XGM_stopPlay();
        waitMs(200);
        XGM_setLoopNumber(-1);
        XGM_startPlay((const u8 *)&mus_intro);
        waitMs(7000);
        VDP_clearPlane(WINDOW, TRUE);
        SPR_reset();
        SPR_end();
        gameState = ID_TITLE;
        return;
    }

    // Bullet Iteration & Collision
    if (bullets_active > 0) {
        for (int i=0; i<AMNT_BULLETS; i++) {
            if (bullets[i].hp <= 0) continue;
            // Check collision
            if(colCheckMap(&bullets[i], FALSE, FALSE)) {
                bullets[i].hp = 0;
                bullets[i].pos.iX = 0;
                bullets[i].pos.iY = 100;
                SPR_setVisibility(bullets[i].spr, HIDDEN);
                bullets_active--;
                continue;
            }
            // Move bullet
            bullets[i].pos.iX += fix16ToInt(bullets[i].vel.fX);
            // Set Position relative to map
            SPR_setPosition(bullets[i].spr, bullets[i].pos.iX-mapPos, bullets[i].pos.iY);
        }
    }
    // Enemy Iteration & Collision
    spawnEnemy();
    if (enemies_active > 0) {
        for (int i=0; i<AMNT_ENEMIES; i++) {
            if (enemies[i].hp <= 0) continue;
            //Check collision with map
            if (colCheckMap(&enemies[i], FALSE, TRUE)) {
                enemies[i].vel.fY = VEL_ENEMY_JMP;
            }
            if (colCheckMap(&enemies[i], TRUE, FALSE)) {
                enemies[i].vel.fX = -enemies[i].vel.fX;
            }
            // Check collision with bullets
            for (int b=0; b<AMNT_BULLETS; b++) {
                if (bullets[b].hp <= 0) continue;
                if ((enemies[i].pos.iX < (bullets[b].pos.iX + bullets[b].dimPx.iX)) && ((enemies[i].pos.iX + enemies[i].dimPx.iX) > bullets[b].pos.iX )) {
                    if ((enemies[i].pos.iY > bullets[b].pos.iY) && ((enemies[i].pos.iY < bullets[b].pos.iY+bullets[b].dimPx.iY))) {
                        bullets[b].hp = 0;
                        bullets[b].pos.iY = 100;
                        SPR_setVisibility(bullets[b].spr, HIDDEN);
                        bullets_active--;

                        enemies[i].hp = 0;
                        enemies[i].pos.iY = 100;
                        SPR_setVisibility(enemies[i].spr, HIDDEN);
                        enemies_active--;

                        player_score++;
                        drawScore();
                    }
                }
            }

            // Check collision with player
            if (!isInvul) {
                if ((enemies[i].pos.iX < (player.pos.iX + player.dimPx.iX)) && ((enemies[i].pos.iX + enemies[i].dimPx.iX) > player.pos.iX )) {
                    if ((enemies[i].pos.iY > player.pos.iY) && ((enemies[i].pos.iY < player.pos.iY+player.dimPx.iY))) {
                        player.hp--;
                        player.vel.fX += (enemies[i].vel.fX*2);
                        isInvul = TRUE;
                        invulTime = getTimeAsFix32(TRUE) + FIX32(INVUL_TIME);
                        drawHealth();
                    }
                }
            }

            // Add velocity
            if (enemies[i].pos.iX < player.pos.iX) {
                enemies[i].vel.fX += SPD_ENEMY_ACC;
                SPR_setHFlip(enemies[i].spr, FALSE);
            } else {
                enemies[i].vel.fX -= SPD_ENEMY_ACC;
                SPR_setHFlip(enemies[i].spr, TRUE);
            }
            // Restrict movement speed
            if (enemies[i].vel.fX > SPD_ENEMY_MAX) enemies[i].vel.fX = SPD_ENEMY_MAX;
            if (enemies[i].vel.fX < -1 * SPD_ENEMY_MAX) enemies[i].vel.fX = -1 * SPD_ENEMY_MAX;

            // Apply Gravity
            enemies[i].vel.fY += GRAVITY/2;

            // Move
            enemies[i].pos.iX += fix16ToInt(enemies[i].vel.fX);
            enemies[i].pos.iY += fix16ToInt(enemies[i].vel.fY);
            SPR_setPosition(enemies[i].spr, enemies[i].pos.iX-mapPos, enemies[i].pos.iY);
        }
    }
    SPR_update();

    // Background Parallax
    for (int i=0; i<4; i++) {
        for (int k=scroll_bg_index[i]; k<(scroll_bg_index[i]+scroll_bg_length[i]); k++)
        {
            scroll_bg_valf[k] = -scroll_bg_shift[i] * mapPos;
        }
    }
    for (int i=0; i<32; i++) { scroll_bg_vali[i] = fix16ToInt(scroll_bg_valf[i]); }
    VDP_setHorizontalScrollTile(BG_B, 0, scroll_bg_vali, 32, CPU);

    //VDP_showFPS(TRUE);
}

// Auxiliary functions
bool colCheckMap(Entity *e, u8 hor, u8 ver) {
    s16 tX, tY;
    u8  tW = e->dimTl.iX;
    u8  tH = e->dimTl.iY;

    if (hor == TRUE) {
        if (e->vel.fX == FIX16(0)) return FALSE;
        tY = e->pos.iY >> 3;
        u8 horDir = (e->vel.fX  > FIX16(0));
        if (horDir) {
            tX = ((e->pos.iX + fix16ToInt(e->vel.fX)) >> 3)+tW;
        } else {
            tX = (e->pos.iX + fix16ToInt(e->vel.fX)) >> 3;
        }
        for (int i=tY; i<(tY+tH); i++) {
            if (level[i][tX] == MAP_SOLID) {
                e->pos.iX = (tX+(horDir ? (-tW) : 1))*8;
                return TRUE;
            }
        }
        return FALSE;
    }

    if (ver == TRUE) {
        if (e->vel.fY == FIX16(0)) return FALSE;
        tX = e->pos.iX >> 3;
        u8 verDir = (e->vel.fY  > FIX16(0));
        e->contact = FALSE;
        if (verDir) {
            tY = ((e->pos.iY + fix16ToInt(e->vel.fY)) >> 3)+tH;
        } else {
            tY = (e->pos.iY + fix16ToInt(e->vel.fY)) >> 3;
        }
        for (int i=tX; i<(tX+tW); i++) {
            if (level[tY][i] == MAP_SOLID) {
                e->pos.iY = (tY+(verDir ? (-tH) : 1))*8;
                if (verDir) e->contact = TRUE;
                return TRUE;
            }
        }
        return FALSE;
    }

    tX = (e->pos.iX >> 3)+(e->dimTl.iX/2);
    tY = (e->pos.iY >> 3)+(e->dimTl.iY/2);
    if (level[tY][tX] == MAP_SOLID) {
        return TRUE;
    }

    return FALSE;
}

void drawScore() {
    sprintf(scoreTxt, "SCORE: %d/%d", player_score, GOAL_SCORE);
    VDP_drawText(scoreTxt, 1, 0);
}

void drawHealth() {
    u8 tx = 30;
    VDP_clearText(tx, 0, 10);
    VDP_drawText("HP: ", tx, 0);
    tx = 34;

    for (int i=0; i<player.hp; i++) {
        VDP_drawText("X", tx, 0);
        tx++;
    }
}

void spawnEnemy() {
    if (abs(mapPos_last - mapPos) > MAP_SPAWN_DISTANCE) {
        if (enemies_active < AMNT_ENEMIES) {
            for (int i=0; i<AMNT_ENEMIES; i++) {
                if (enemies[i].hp == 0) { // Unused enemy found
                    u8 r = (random() % (10-1+1)+1);
                    enemies[i].pos.iX = player.pos.iX + (r>5 ? 160 : -160);
                    enemies[i].pos.iY = 20;
                    enemies[i].hp = 1;
                    SPR_setVisibility(enemies[i].spr, AUTO_FAST);
                    SPR_setHFlip(enemies[i].spr, (r>5 ? FALSE : TRUE));
                    enemies_active++;
                    break;
                }
            }
        }
        mapPos_last = mapPos;
    }
}

void fireGun() {
    if (bullets_active < AMNT_BULLETS) {
        for (int i=0; i<AMNT_BULLETS; i++) {
            if (bullets[i].hp == 0) { // Unused bullet found
                bullets[i].pos.iX = player.pos.iX+player.dimPx.iX/2;
                bullets[i].pos.iY = player.pos.iY+player.dimPx.iY/2-5;
                bullets[i].vel.fX = FIX16(SPD_BULLET * player_last_dir);
                bullets[i].hp = 1;
                SPR_setVisibility(bullets[i].spr, AUTO_FAST);
                SPR_setHFlip(bullets[i].spr, (player_last_dir>0 ? FALSE : TRUE));
                bullets_active++;
                break;
            }
        }
    }
}

// Function arrays
void (*setup_func[3])()  = { &setup_titlescreen,  &setup_intro,  &setup_game };
void (*update_func[3])() = { &update_titlescreen, &update_intro, &update_game };

// Main function
int main()
{
    while(1)
    {
        if (gameState != gameState_last) {
            (*setup_func[gameState])();
            gameState_last = gameState;
        }
        (*update_func[gameState])();
        SYS_doVBlankProcess();
    }
    return (0);
}
