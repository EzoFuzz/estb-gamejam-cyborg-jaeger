#include <genesis.h>

#include <art.h>
#include <lvl.h>
#include <level.h>
#include <mus.h>

#include <state.h>

// Input handlers
void input_intro( u16 joy, u16 changed, u16 state) {
    if (state & (BUTTON_START | BUTTON_A | BUTTON_B | BUTTON_C)) {
        gameState = ID_GAME;
    }
}

// Setup functions
void setup_intro() {
    SYS_disableInts();

    JOY_reset();
    JOY_setEventHandler(&input_intro);

    PAL_setPalette(PAL0, img_cloud01.palette->data, DMA);
    PAL_setPalette(PAL1, spr_char.palette->data, DMA);
    PAL_setPalette(PAL2, spr_enemy.palette->data, DMA);
    VDP_setTextPalette(PAL1);
    VDP_setBackgroundColor(4);

    VDP_setPlaneSize(64,32,TRUE);
    VDP_setWindowVPos(FALSE, -1);
    VDP_setTextPlane(BG_B);

    VDP_clearPlane(BG_A, TRUE);
    VDP_clearPlane(BG_B, TRUE);

    VDP_drawText("Objective:", 1, 5);
    VDP_drawText("Exterminate all the wild red monkeys!", 1, 6);
    VDP_drawText("Keep your distance, they will chase!", 1, 7);
    VDP_drawText("You have three hit points.", 1, 8);

    VDP_drawText("Controls:", 1, 10);
    VDP_drawText("D-PAD:     Move", 1, 11);
    VDP_drawText("A-BUTTON:  Jump", 1, 12);
    VDP_drawText("B-BUTTON:  Shoot", 1, 13);

    VDP_drawText("Good luck, have fun :)", 9, 16);

    VDP_drawText("Press any button...", 10, 18);

    XGM_stopPlay();
    SYS_enableInts();
}

// Update functions
void update_intro() {

}