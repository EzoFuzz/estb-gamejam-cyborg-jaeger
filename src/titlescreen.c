#include <genesis.h>

#include <art.h>
#include <lvl.h>
#include <level.h>
#include <mus.h>

#include <state.h>

// Input handlers
void input_titlescreen( u16 joy, u16 changed, u16 state) {
    if (state & BUTTON_START) {
        gameState = ID_INTRO;
    }
}

// Setup functions
void setup_titlescreen() {
    SYS_disableInts();

    JOY_init();
    JOY_setEventHandler(&input_titlescreen);

    PAL_setPalette(PAL0, img_cloud01.palette->data, DMA);
    PAL_setPalette(PAL1, spr_char.palette->data, DMA);
    PAL_setPalette(PAL2, spr_enemy.palette->data, DMA);
    VDP_setTextPalette(PAL1);
    VDP_setBackgroundColor(38);

    VDP_setScrollingMode(HSCROLL_PLANE, VSCROLL_PLANE);
    VDP_setPlaneSize(64,32,TRUE);
    VDP_setWindowVPos(FALSE, -1);
    VDP_setTextPlane(BG_B);
    

    VDP_clearPlane(BG_A, TRUE);
    VDP_clearPlane(BG_B, TRUE);
    VDP_loadTileSet(img_title.tileset, TILE_USERINDEX, DMA);
    VDP_drawImageEx(BG_B, &img_title, TILE_ATTR_FULL(PAL2, FALSE, FALSE, FALSE, TILE_USERINDEX), 4, 9, FALSE, TRUE);

    VDP_drawText("PRESS START!", 14, 18);
    VDP_drawText("MEGA DRIVE GAME JAM - BACHELOR", 0, 27);

    XGM_stopPlay();
    XGM_setLoopNumber(-1);
    XGM_startPlay((const u8 *)&mus_level);

    SYS_enableInts();
}

// Update functions
void update_titlescreen() {

}