#ifndef _RES_ART_H_
#define _RES_ART_H_

extern const SpriteDefinition spr_char;
extern const SpriteDefinition spr_enemy;
extern const SpriteDefinition spr_blaster;
extern const Image img_title;
extern const Image img_mountains;
extern const Image img_cloud01;
extern const Image img_cloud02;
extern const Image img_cloud03;

#endif // _RES_ART_H_
