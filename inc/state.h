#ifndef _STATE_H_
#define _STATE_H_

#include <genesis.h>
typedef enum {
    ID_TITLE,
    ID_INTRO,
    ID_GAME
} GameStates;

extern u8 gameState;
extern u8 gameState_last;

void input_titlescreen( u16 joy, u16 changed, u16 state);
void setup_titlescreen();
void update_titlescreen();

void input_intro( u16 joy, u16 changed, u16 state);
void setup_intro();
void update_intro();

void input_game( u16 joy, u16 changed, u16 state);
void setup_game();
void update_game();

#endif // _STATE_H_